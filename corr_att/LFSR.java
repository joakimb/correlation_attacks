package corr_att;

import java.util.Arrays;



public class LFSR {
	public byte[] state;
	public int[] cp;

	public LFSR(byte[] state, int[] cp){
		if (state != null)
			this.state = Arrays.copyOf(state, state.length);
		this.cp = cp;
	}
	
	public byte clock(){
		byte output = state[0];
		byte nextInput = 0;
		for (int s : cp) {
			nextInput += state[s];
		}
		nextInput %= 2;
		// det är snabbare att ha state i en länkad lista och bara flytta head, se om vi orkar fixa det
		for (int i = 0; i < state.length - 1; i++){
			state[i] = state[i+1];
		}
		state[state.length-1] = nextInput;
		return output;
	} 
	
	public byte[] clockNTimes(int n){
		byte[] bits = new byte[n];
		for (int i = 0; i < n; i++){
			bits[i] = clock();
		}
		return bits;
	}
	public void setState(byte[] state){
		this.state = Arrays.copyOf(state, state.length);
	}
}
